# Mastermind

Mastermind is a little puzzle game where you have to find the hidden code. The code consists of four random colors.
Your mission is to find these four colors as fast as possible!

## Project setup
```
yarn install

yarn serve
```

## Rules

1. The game starts by displaying four random colors. These colors are selected out of an array of six colors, so each game is going to be different!

2. In order to find the correct code, you have to guess which color matches it's correct position.

3. Select one out of four options for each position and submit your guess.

4. Your guess will be validated and the game will show which color was correct or incorrect.

5. Submitting more guess will result in finding the correct code! But don't forget to keep eye on the timer! You will have 5 minutes to find the hidden code. 

#### Good luck!
